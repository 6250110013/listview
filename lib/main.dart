import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  final titles = [
    'bike',
    'boat',
    'bus',
    'car',
    'railway',
    'run',
    'subway',
    'transit',
    'walk'
  ];
  final images = [
    'assets/images/bike.jpg',
    'assets/images/boat.jpg',
    'assets/images/bus.jpg',
    'assets/images/car.jpg',
    'assets/images/railway.jpg',
    'assets/images/run.jpg',
    'assets/images/subway.jpg',
    'assets/images/transit.jpg',
    'assets/images/walk.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Basic ListView',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Listview'),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(
                    backgroundImage: AssetImage(images[index]),
                  ),
                  title: Text(
                    '${titles[index]}',
                    style: TextStyle(fontSize: 18),
                  ),
                  subtitle: Text(
                    'There are many passengers in serveral vehicles',
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                  trailing: Icon(
                    Icons.notifications_none,
                    size: 25,
                  ),
                  onTap: () {
                    AlertDialog alert = AlertDialog(
                      title: Text('Welcome'),
                      // To display the title it is optional
                      content: Text('This is a ${titles[index]}'),
                      // Message which will be pop up on the screen
                      // Action widget which will provide the user to acknowledge the choice
                      actions: [
                        ElevatedButton(
                          // FlatButton widget is used to make a text to work like a button
                          onPressed: () => Navigator.pop(context, false),
                          // function used to perform after pressing the button
                          child: Text('CANCEL'),
                        ),
                        ElevatedButton(
                          onPressed: () => Navigator.pop(context, true),
                          child: Text('ACCEPT'),
                        ),
                      ],
                    );
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return alert;
                      },
                    );
                  },
                ),
                Divider(
                  thickness: 0.8,
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
